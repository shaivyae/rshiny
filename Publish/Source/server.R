server<-function(session,input, output) {
  
  if (!interactive()) sink(stderr(), type = "output")
  
  Raw_Data_inputs<-reactive({
    app_dir <- file.path(Base_dir,"Publish/Data",input$application)
    raw_input<-readRDS(sprintf("%s/raw_input.rds",app_dir))
    raw_input$Time<-as.POSIXct(raw_input$Time)
    rownames(raw_input)<-raw_input$Time
    filtered<-readRDS(sprintf("%s/filtered.rds",app_dir))
    list(raw_input=raw_input,filtered=filtered)
  })   
  
  Peak_data_input<-reactive({
    app_dir <- file.path(Base_dir,"Publish/Data",input$application)
    Peak_input<-readRDS(sprintf("%s/peak_data.rds",app_dir))
    Peak_input$Time<-as.POSIXct(Peak_input$Time)
    Peak_Result<-readRDS(sprintf("%s/peak_result.rds",app_dir))
    corelation_input<-readRDS(sprintf("%s/txn_metric_plot_order.rds",app_dir))
    names(Peak_input)<-gsub("Flag_Peaks_","",names(Peak_input))
    corelation_input[,2]<-gsub("Flag_Peaks_","",corelation_input[,2])
    list(Peak_input=Peak_input,Peak_Result=Peak_Result,corelation_input=corelation_input)
  })
  
  summary_input<-reactive({
    summary_df<-read.csv(sprintf("%s/Publish/Data/summary_df.csv",Base_dir))
    App_Refresh_Summary<-read.csv(sprintf("%s/Publish/Data/App_Refresh_Summary.csv",Base_dir))
    list(summary_df=summary_df,App_Refresh_Summary=App_Refresh_Summary)
  })
  
  source(sprintf("%s/server/Summary_Tab.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Drill_Down_Tab.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Heat_Map.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Overview_Tab.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/App_Scope.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Notifications.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Alerts_Tab.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Forecast.R",Source_dir),local = TRUE, encoding = "UTF-8")
  source(sprintf("%s/server/Capacity_Tab.R",Source_dir),local = TRUE, encoding = "UTF-8")
  
  
}
