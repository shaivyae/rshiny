stl_process<-function(data){
  data[is.na(data)]<-0
  cols<-names(data)[!names(data) %in% 'Time']
  for (col in cols){
      print(col)
      ts_obj <- ts(data[,col], frequency = 24)
      if(length(ts_obj)>0)
      {
        err<-stl(ts_obj,'periodic')
        err_rem<-data.frame(err$time.series)
        r<-data.frame(data$Time,err_rem$remainder)
        names(r)<-c('Time',col)
        if(col==cols[1]){
          result=r
        }else{
          result=merge(result,r,by='Time',all=T)
        }
      }
    }
}

peak_data <- ProfileAppAnomalies(result,names(result)[-c(1,grep('Txn_',names(result)))],NULL,NULL,NULL,1,'RIB',2)
