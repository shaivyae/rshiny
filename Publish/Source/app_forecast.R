library(ggplot2)
library(zoo)
library(xts)
library(plotly)
library(reshape2)
library(forecast)
library(timeSeries)
library(taRifx)
library(lubridate)

app_forecast<-function(sor,nhead,fromtime,totime,colname)
{
  dd<-read.csv(sprintf('%s.csv',sor))
  #dd$Date<-as.POSIXct(dd$Date)
  for (i in colnames(dd))
  {
    dd[,i]<-as.character(dd[,i])
  }
  for (i in colnames(dd)[-1])
  {
    dd[,i]<-as.numeric(dd[,i])
  }
  y<-dd[complete.cases(dd),]
  ampm<-grep('AM|PM',y$Date)
  y$Date[ampm]<-as.POSIXct(y$Date[ampm], format="%m/%d/%Y %I:%M:%S %p")
  rest<-grep('-',y$Date)
  r<-as.POSIXct(y$Date[rest[1]], format="%Y-%d-%m %H:%M")
  if(is.na(r))
  {
    y$Date[rest]<-as.POSIXct(y$Date[rest], format="%m-%d-%Y %H:%M")
  }
  else{
    y$Date[rest]<-as.POSIXct(y$Date[rest], format="%Y-%d-%m %H:%M")
  }
  y$Date<-as.POSIXct(as.numeric(y$Date),origin =  '1970-01-01')
  hour_wise<-with( y , y[ hour( Date ) >= as.numeric(fromtime) & hour( Date ) <= as.numeric(totime) , ] )
  #w<-(totime-fromtime)*1
  #day_wise_data <- rollapply(hour_wise[,-1],width=w,by=w,FUN=sum)
  day_wise<-rowsum(hour_wise[,-1], format(hour_wise$Date, '%Y-%m-%d'))
  data_ts<-ts(day_wise[,colname],frequency = 7)
  extra<-seq(as.Date(max(rownames(day_wise)))+1,as.Date(max(rownames(day_wise)))+nhead,length.out=nhead)
  first<-as.Date(rownames(day_wise))
  t_ind <- c(first,as.Date(extra))
  app_HWplot(data_ts, n.ahead=nhead, t_ind = t_ind,col=paste0(sor,colname))
}

app_HWplot<-function(ts_object,  n.ahead=10,  CI=.95,  error.ribbon='green', line.size=1, t_ind,col){
  hw_object<-HoltWinters(ts_object,beta=FALSE)
  forecast<-predict(hw_object,  n.ahead=n.ahead,  prediction.interval=T,  level=CI)
  for_values<-data.frame(time=round(time(forecast),  3),  value_forecast=as.data.frame(forecast)$fit,  dev=as.data.frame(forecast)$upr-as.data.frame(forecast)$fit)
  fitted_values<-data.frame(time=round(time(hw_object$fitted),  3),  value_fitted=as.data.frame(hw_object$fitted)$xhat)
  actual_values<-data.frame(time=round(time(hw_object$x),  3),  Actual=c(hw_object$x))
  actual_values[["Time"]] <- t_ind[1:nrow(actual_values)]
  graphset<-merge(actual_values,  fitted_values,  by='time',  all=TRUE)
  graphset<-merge(graphset,  for_values,  all=TRUE,  by='time')
  graphset[is.na(graphset$dev),  ]$dev<-0
  graphset$Fitted<-c(rep(NA,  NROW(graphset)-(NROW(for_values) + NROW(fitted_values))),  fitted_values$value_fitted,  for_values$value_forecast)
  #graphset <- cbind(t_ind,graphset)
  orig <- max(t_ind[1:nrow(actual_values)])
  graphset[["Time"]] <-as.Date(t_ind[1:nrow(graphset)],origin=orig)
  graphset<-graphset[,-5]
  #write.csv(graphset,file=paste("Dumps/fitted_actual_HW_",n.ahead,".csv",sep=""))
  graphset.melt<-melt(graphset[, c('Time','Actual', 'Fitted')], id=c('Time'))
  p<-ggplot(graphset.melt,  aes(x=Time,  y=value)) + geom_ribbon(data=graphset, aes(x=Time, y=Fitted, ymin=Fitted-dev,  ymax=Fitted + dev),  alpha=.2,  fill=error.ribbon) + geom_line(aes(colour=variable), size=line.size) + geom_vline(aes(xintercept=as.numeric(max(actual_values$Time))),  lty=2) + 
    labs(title = col)+xlab('Time') + ylab('Value')+theme(plot.margin = unit(c(0, 0, 2, 0), "cm"))
  ggp<-ggplotly(p)
  htmlwidgets::saveWidget(as.widget(ggp), sprintf("%s.html",paste0(getwd(),'/',col,'_',Sys.Date())))
  return(ggplotly(p,width=1100,height=400))
}
