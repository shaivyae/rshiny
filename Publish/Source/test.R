test_data<-fetch_kpi_data(host_ip,
                         '2017-02-01',
                         '2017-03-30',
                         1022,810,744,749,
                         host_kpi_cols,apache_kpi_cols,oracle_kpi_cols,websphere_kpi_cols,NULL,'RIB')

txn_and_kpi<-fetch_data(host_ip,
                        '2017-02-01',
                        '2017-03-30',
                        44,
                        txn_kpis,
                        Txn_BaselineSearchSpace,
                        1022,810,744,749,
                        host_kpi_cols,apache_kpi_cols,oracle_kpi_cols,websphere_kpi_cols,NULL,'RIB')

col<-'THREADPOOL_ACTIVECOUNT'
TAC<-test_data[,c('Time',col)]
TAC$Time<-as.POSIXct(TAC$Time)
t1<-Sys.time()
index<-data.frame(Time=seq(min(test_data$Time), max(test_data$Time),by=60))
TAC<-merge(index,TAC,all=TRUE,by='Time')
TAC<-do(TAC,col,'SMA',5)
print(difftime(Sys.time(),t1))

t1<-Sys.time()
win_len<-120
peak_refresh<-ProfileAppAnomalies(TAC,FALSE,col,NULL,NULL,win_len,NULL,'RIB')
print(Sys.time()-t1)
t1<-Sys.time()
filtered<-as.data.frame(rollapply(TAC[,names(TAC)!="Time"],width=win_len,by=win_len,FUN=mean))
names(filtered)<-names(TAC)[-1]
Plot_Peaks(filtered,peak_refresh,peak_refresh$Time,col,paste("Time Vs",col),'Count')
print(Sys.time()-t1)