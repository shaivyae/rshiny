output$forecast_plot<-renderPlotly({
  if((nchar(input$aid) >=1 || nchar(input$query)>1) & (input$button || input$button1) & length(input$lists)>=1)
  { 
    if(input$button){
      data<-fr_data()$data_time
      day_wise_data<-fr_data()$day_wise_data
    }
    else
    {  data<-fr_data1()$data_time
    day_wise_data<-fr_data1()$day_wise_data
    }
    if(!is.null(data) & nrow(data)>0)
    { forecast<-as.numeric(input$frct)
    if(as.character(input$freq)=='Weekly')
    {
      freqy<-7
    }
    else if(as.character(input$freq)=='Monthly')
    {
      freqy<-30
    }
    else{
      freqy<-12
    }
    col<-as.character(input$lists)
    data_ts<-ts(day_wise_data[,col], start=as.Date(min(data$Time)), frequency=freqy)
    extra<-seq(as.Date(max(data$Time))+1,as.Date(max(data$Time))+forecast,length.out=forecast)
    first<-unique(as.Date(as.POSIXlt(strptime(data$Time, format="%Y-%m-%d"))))
    t_ind <- c(first,as.Date(extra))
    #t_ind <- seq(as.Date(min(data$Time)),as.Date(max(data$Time))+forecast,length.out=length(data_ts)+forecast)
    HWplot(data_ts, n.ahead=forecast, t_ind = t_ind)
    }
  }
  else{
    return(NULL)}
})

output$fr_dates <- renderUI({
  dateRangeInput("fr_dates", label = "Date range",start = Sys.Date()-180,end = Sys.Date())
})

output$valuee <- renderPrint({
  if((nchar(input$aid) >=1 || nchar(input$query)>1) & (input$button || input$button1))
  {
    print("Sample Query Output")
    if(input$button){data<-fr_data()$fr_data}else{data<-fr_data1()$fr_data}
    print(head(data))
    print(paste("Dimension OF Query Output",dim(data)[1],"*",dim(data)[2]))
  }
})

output$lists<-renderUI({
  if((nchar(input$aid) >=1 || nchar(input$query)>1) & (input$button || input$button1))
  {
    if(input$button){data<-fr_data()$fr_data}else{data<-fr_data1()$fr_data}
    kpi_list<-names(data)
    kpi_list<-kpi_list[!kpi_list %in% c("MiningClusterId","BMiningClusterId","Transaction_Cluster_Id","Time","CompInstanceId")]
    selectizeInput(inputId = "lists",label = "KPIS", choices = kpi_list,
                   options = list(maxItems = 1,
                                  placeholder = 'Select Kpi'),selected=kpi_list[1])
  }
})

output$aid_text <- reactive({
  if(input$oob_comp=='Transaction'){
    paste("Enter Application Instance Id")}
  else{
    paste("Enter CompInstance Instance Id")
  }
})

observeEvent(input$sel,if(input$sel == 'oob'){
  toggle(id = "advanced", anim = TRUE)
  shinyjs::hide("advanced1")
  shinyjs::show("aid")
  shinyjs::show("fr_dates")
  shinyjs::enable("button")
  shinyjs::disable("button1")
})

observeEvent(input$sel,if(input$sel == 'cust'){
  print("Custom")
  toggle(id = "advanced1", anim = TRUE)
  shinyjs::hide("advanced")
  shinyjs::hide("aid")
  shinyjs::hide("fr_dates")
  shinyjs::enable("button1")
  shinyjs::disable("button")
})

observeEvent(input$button,{
  if(nchar(input$aid)==0){
    observe({        
      session$sendCustomMessage(type = 'testmessage',
                                message = list("Please enter Instance Id!!!!"))
    })
  }
})

fr_data<-eventReactive(input$button,{
  pb <- tkProgressBar(title = "progress bar", min = 0,
                      max = 100, width = 300)
  apid<-as.numeric(input$aid)
  from<-as.Date(input$fr_dates[[1]])
  to<-as.Date(input$fr_dates[[2]])
  if(input$oob_comp == 'Transaction'){
    q<-sprintf("select CONVERT_TZ(Time,'+0:00','+5:30') Time,Total from TRANSACTIONCOLL where AppInstanceId=%s and CONVERT_TZ(Time,'+0:00','+5:30') between '%s 00:00:00' and '%s 23:59:00'",apid,from,to)
  }
  else if (input$oob_comp == 'Apache'){
    q<-sprintf("select CONVERT_TZ(Time,'+0:00','+5:30') Time,TOTAL_ACCESSES,BUSY_WORKERS,IDLE_WORKERS,CPU_LOAD,TOTAL_REQUESTS,TOTAL_KBYTES_PER_REQUEST,TOTAL_KBYTES_SERVED from ApacheMetricsCOLL where CompInstanceId=%s and CONVERT_TZ(Time,'+0:00','+5:30') between '%s 00:00:00' and '%s 23:59:00'",apid,from,to)
  }
  else if (input$oob_comp == 'Oracle') {
    q<-sprintf("select CONVERT_TZ(Time,'+0:00','+5:30') Time, BYTES_RECEIVED,BYTES_SENT,CURRENT_OPEN_CURSORS,CURRENT_LOGONS,CACHE_HITRATIO,PHYSICAL_READS,PHYSICAL_WRITES,LOGICAL_READS,USER_COMMITS,USER_CALLS,USER_ROLLBACKS,EXECUTE_COUNT,NUMBEROF_SESSIONS,CPU_USAGE,HARD_PARSECOUNT,SORTS_DISK,SORTS_ROWS,TABLESCANS_LONG,TABLESCANS_SHORT,Locks_Waits from OracleMetricsCOLL where CompInstanceId=%s and CONVERT_TZ(Time,'+0:00','+5:30') between '%s 00:00:00' and '%s 23:59:00'",apid,from,to)
  }
  else if (input$oob_comp == 'WAS'){
    q<-sprintf("select CONVERT_TZ(Time,'+0:00','+5:30') Time,JVM_HEAPSIZE,JVM_USEDMEMORY ,JVM_CPUUSAGE,THREADPOOL_CREATECOUNT  ,THREADPOOL_DESTROYCOUNT ,THREADPOOL_ACTIVECOUNT  ,THREADPOOL_POOLSIZE  ,CONNECTIONPOOL_POOLSIZE ,CONNECTIONPOOL_FREEPOOLSIZE,CONNECTIONPOOL_PERCENTUSED ,CONNECTIONPOOL_WAITTIME ,CONNECTIONPOOL_CREATECOUNT ,CONNECTIONPOOL_CLOSECOUNT  ,CONNECTIONPOOL_WAITINGTHREADCOUNT,CONNECTIONPOOL_USETIME  ,TOTAL_TRANSACTIONS_ACTIVE  ,TOTAL_TRANSACTIONS_COMMITTED  ,TOTAL_TRANSACTIONS_ROLLEDBACK ,TOTAL_SESSIONS_CREATED  ,TOTAL_SESSIONS_INVALIDATED ,TOTAL_SESSIONS_LIVE  ,TOTAL_SERVLETS_LOADED,TOTAL_SERVLETS_RELOADED ,EJB_CREATED ,EJB_REMOVED ,EJB_PASSIVATED ,EJB_METHODCALL ,EJB_METHODRESPONSE_TIME from WebsphereMetricsCOLL where CompInstanceId=%s and CONVERT_TZ(Time,'+0:00','+5:30') between '%s 00:00:00' and '%s 23:59:00'",apid,from,to)
  }
  else {
    q<-sprintf("select CONVERT_TZ(Time,'+0:00','+5:30') Time,CPU_UTIL,MEM_UTIL,DISK_IOREAD,DISK_IOWRITE,EC_CPU_UTIL,LOAD_AVERAGE from LinuxHostMetricsCOLL Where CompInstanceId=%s and CONVERT_TZ(Time,'+0:00','+5:30') between '%s 00:00:00' and '%s 23:59:00'",apid,from,to)
  }
  samp<-sample(10:20,1)
  setTkProgressBar(pb, samp, title=paste( samp,"% done"))
  fr_data<-as.data.frame(dbGetQuery(con,q),row.names=NULL)
  samp<-sample(50:60,1)
  setTkProgressBar(pb, samp, title=paste( samp,"% done"))
  if(nrow(fr_data)>0)
  {
    fr_data[is.null(fr_data)]<-0
    fr_data[is.na(fr_data)]<-0
    sq<-data.frame(Time=seq(as.POSIXct(min(fr_data$Time)),as.POSIXct(max(fr_data$Time)),by="min"))
    samp<-sample(61:70,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    fr_data$Time<-as.POSIXct(fr_data$Time)
    fr_data<-merge(fr_data,sq,by="Time",all.y=T)
    fr_data<-do(fr_data,names(fr_data)[-1],"SMA",5)
    samp<-sample(71:75,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    fr_data<-with( fr_data , fr_data[ hour( Time ) >= 9 & hour( Time ) < 19 , ] )
    func_perc <- function(x, q=0.98) {
      return(quantile(x, q))
    }
    day_wise_data <- rollapply(fr_data[,-1],width=600,by=600,FUN=func_perc)
    samp<-sample(76:80,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    data_time<-data.frame(Time=fr_data$Time)
    close(pb)
    list(fr_data=fr_data,day_wise_data=day_wise_data,data_time=data_time) 
  }
  else{
    close(pb)
    observe({        
      session$sendCustomMessage(type = 'testmessage',
                                message = list("No data!!!!"))
      list(fr_data=NULL,day_wise_data=NULL,data_time=NULL)
      
    })
  }
})

fr_data1<-eventReactive(input$button1,{
  print("hi")
  if(nchar(input$query)>1)
  { 
    data<-NULL
    pb <- tkProgressBar(title = "progress bar", min = 0,
                        max = 100, width = 300)
    tryCatch({
      q<-as.character(input$query)
      samp<-sample(10:20,1)
      setTkProgressBar(pb, samp, title=paste( samp,"% done"))
      data<-as.data.frame(dbGetQuery(con,q),row.names=NULL)
      fr_data<-data[!names(data) %in% c("MiningClusterId","BMiningClusterId","Transaction_Cluster_Id","CompInstanceId")]
      samp<-sample(50:60,1)
      setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    },
    error=function(e) {
      observe({ 
        close(pb)
        session$sendCustomMessage(type = 'testmessage',
                                  message = list("Please enter the right query!!!!"))
      })
    })
    fr_data[is.null(fr_data)]<-0
    fr_data[is.na(fr_data)]<-0
    sq<-data.frame(Time=seq(as.POSIXct(min(fr_data$Time)),as.POSIXct(max(fr_data$Time)),by="min"))
    samp<-sample(61:70,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    fr_data$Time<-as.POSIXct(fr_data$Time)
    fr_data<-merge(fr_data,sq,by="Time",all.y=T)
    fr_data<-do(fr_data,names(fr_data)[-1],"SMA",5)
    samp<-sample(71:75,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    fr_data<-with( fr_data , fr_data[ hour( Time ) >= 9 & hour( Time ) < 19 , ] )
    func_perc <- function(x, q=0.98) {
      return(quantile(x, q))
    }
    day_wise_data <- rollapply(fr_data[,-1],width=600,by=600,FUN=func_perc)
    samp<-sample(76:85,1)
    setTkProgressBar(pb, samp, title=paste( samp,"% done"))
    data_time<-data.frame(Time=fr_data$Time)
    close(pb)
    list(fr_data=fr_data,day_wise_data=day_wise_data,data_time=data_time) 
  }
  else{return(NULL)}
})

HWplot<-function(ts_object,  n.ahead=10,  CI=.95,  error.ribbon='green', line.size=1, t_ind){
  hw_object<-HoltWinters(ts_object,seasonal = "mult",beta=FALSE)
  forecast<-predict(hw_object,  n.ahead=n.ahead,  prediction.interval=T,  level=CI)
  for_values<-data.frame(time=round(time(forecast),  3),  value_forecast=as.data.frame(forecast)$fit,  dev=as.data.frame(forecast)$upr-as.data.frame(forecast)$fit)
  fitted_values<-data.frame(time=round(time(hw_object$fitted),  3),  value_fitted=as.data.frame(hw_object$fitted)$xhat)
  actual_values<-data.frame(time=round(time(hw_object$x),  3),  Actual=c(hw_object$x))
  actual_values[["Time"]] <- t_ind[1:nrow(actual_values)]
  graphset<-merge(actual_values,  fitted_values,  by='time',  all=TRUE)
  graphset<-merge(graphset,  for_values,  all=TRUE,  by='time')
  graphset[is.na(graphset$dev),  ]$dev<-0
  graphset$Fitted<-c(rep(NA,  NROW(graphset)-(NROW(for_values) + NROW(fitted_values))),  fitted_values$value_fitted,  for_values$value_forecast)
  #graphset <- cbind(t_ind,graphset)
  orig <- max(t_ind)
  graphset[["Time"]] <-as.Date(t_ind,origin=orig)
  graphset<-graphset[,-5]
  #write.csv(graphset,file=paste("Dumps/fitted_actual_HW_",n.ahead,".csv",sep=""))
  graphset.melt<-melt(graphset[, c('Time','Actual', 'Fitted')], id=c('Time'))
  chart_width <- 1100
  chart_height <- 450
  p<-ggplot(graphset.melt,  aes(x=Time,  y=value)) + geom_ribbon(data=graphset, aes(x=Time, y=Fitted, ymin=Fitted-dev,  ymax=Fitted + dev),  alpha=.2,  fill=error.ribbon) + geom_line(aes(colour=variable), size=line.size) + geom_vline(aes(xintercept=as.numeric(max(actual_values$Time))),  lty=2) + xlab('Time') + ylab('Value')
  return(ggplotly(p,width = chart_width, height = chart_height))
}

sma_or_ema <- function(x,f,method,n)
{   
  count<-nrow(x)
  ## Checks if the first element is NA if yes then replace with 0
  if(is.na(x[1,f])=="TRUE")
  {x[1,f]<-0
  }
  if(count>n)
  {
    ### Method is SMA
    if(method=="SMA")
    { 
      ## For loop starts if the values are less than n
      for (i in 2:count)
      { 
        if(is.na(x[i,f])=="TRUE")
        {
          if(i<=n)
          {
            x[i,f]<-mean(x[1:(i-1),f])
          }
          ### if the values are greater than n
          else
          {
            x[i,f]<-mean(x[(i-n):(i-1),f])
          }
        }
      }  
    }
    
    ### Method is EMA	
    else
    {
      mul<-(2/(n+1))
      ## For loop starts if the values are less than n
      for (i in 2:count)
      { 
        if(is.na(x[i,f])=="TRUE")
        {
          if(i<=n)
          {
            x[i,f]<-mean(x[1:(i-1),f])
          }
          ### if the values are greater than n
          else
          {
            if(i==n+1)
            {	avg<-mean(x[(i-n):(i-1),f])
            x[i,f]<-mul*(x[i-1,f]-avg)+avg
            }
            else
            {
              x[i,f]<-(mul*(x[i-1,f]-x[i-2,f])+x[i-2,f])
            }
          }
        }
      }
    }
  }
  x[,f]<-x[,f]
}

do<-function(x,cols,method,n)
{ 
foreach (f=cols) %do% 
{
  x[,f]<-sma_or_ema(x,f,method,n)
}
return(x)
}