##### Drill_Down_Indicators Tab
output$kpis<-renderUI({
  #corelation<-data.frame(Peak_data_input()$corelation_input)
  Peak_Data<-data.frame(Peak_data_input()$Peak_input)
  cc<-names(Peak_Data)[-c(1,grep('Txn',names(Peak_Data)))]
  selectizeInput('kpis', 'KPIs List', choices =cc,multiple = TRUE, options = list(maxItems = 1),selected=cc[1])
})

kpi_name<-reactive({
  kpi<-as.character(input$kpis)
  return(kpi)
})

output$kpiplot<-renderDygraph({
  kpi<-kpi_name()
  filtered<-data.frame(Raw_Data_inputs()$filtered)
  Peak_Data<-data.frame(Peak_data_input()$Peak_input)
  Peak_Result<-data.frame(Peak_data_input()$Peak_Result)
  unit<-as.character(subset(KPI_Data,KPI_NAME==kpi,5))
  dyg<-shade_range(filtered,Peak_Result,Peak_Result$Time,kpi,paste("Time Vs",kpi),unit)
})

output$plot1<-renderDygraph({
  kpi<-kpi_name()
  filtered<-data.frame(Raw_Data_inputs()$filtered)
  Peak_Data<-data.frame(Peak_data_input()$Peak_input)
  Peak_Result<-data.frame(Peak_data_input()$Peak_Result)
  corelation<-data.frame(Peak_data_input()$corelation_input)
  if(input$kpi_input=="Transactions")
  {
    txns<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    txns<-txns[which(txns %in% names(Peak_Data))]
    txns<-txns[grep("TXN|Txn",txns)[1:5]]
    #txns<-txns[which(txns %in% names(Peak_Data))]
    total<-txns[grep("Total",txns)]
    dyg<-Plot_Peaks(filtered,Peak_Data,Peak_Data$Time,total,"Time Vs Transaction Total","Count")
  }
  else
  {
    kpis<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    kpis<-kpis[which(kpis %in% names(Peak_Data))]
    kpis<-kpis[!grepl("TXN|Txn",kpis)][1:4]
    if(length(kpis)>=1 && (!is.na(kpis[1])))
    {
      unit<-as.character(subset(KPI_Data,KPI_NAME==kpis[1],5))
      dyg<-shade_range(filtered,Peak_Result,Peak_Result$Time,kpis[1],paste("Time Vs",kpis[1]),unit)
    }
    else{return(NULL)}
  }
})

output$plot2<-renderDygraph({
  kpi<-kpi_name()
  filtered<-data.frame(Raw_Data_inputs()$filtered)
  Peak_Data<-data.frame(Peak_data_input()$Peak_input)
  Peak_Result<-data.frame(Peak_data_input()$Peak_Result)
  corelation<-data.frame(Peak_data_input()$corelation_input)
  if(input$kpi_input=="Transactions")
  {
    txns<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    txns<-txns[which(txns %in% names(Peak_Data))]
    txns<-txns[grep("TXN|Txn",txns)[1:5]]
    # txns<-txns[which(txns %in% names(Peak_Data))]
    avg<-txns[grep("AvgRespTime",txns)]
    dyg<-Plot_Peaks(filtered,Peak_Data,Peak_Data$Time,avg,"Time Vs Transaction Average Response Time","Response Time")
  }
  else
  {
    kpis<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    kpis<-kpis[which(kpis %in% names(Peak_Data))]
    kpis<-kpis[!grepl("TXN|Txn",kpis)][1:4]
    if(length(kpis)>=1 && (!is.na(kpis[2])))
    {
      unit<-as.character(subset(KPI_Data,KPI_NAME==kpis[2],5))
      dyg<-shade_range(filtered,Peak_Result,Peak_Result$Time,kpis[2],paste("Time Vs",kpis[2]),unit)
    }
    else{return(NULL)}
  }
})
output$plot3<-renderDygraph({
  kpi<-kpi_name()
  filtered<-data.frame(Raw_Data_inputs()$filtered)
  Peak_Data<-data.frame(Peak_data_input()$Peak_input)
  Peak_Result<-data.frame(Peak_data_input()$Peak_Result)
  corelation<-data.frame(Peak_data_input()$corelation_input)
  if(input$kpi_input=="Transactions")
  {
    txns<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    txns<-txns[which(txns %in% names(Peak_Data))]
    txns<-txns[grep("TXN|Txn",txns)[1:5]]
    
    slow<-txns[grep("SlowPercentage",txns)]
    dyg<-Plot_Peaks(filtered,Peak_Data,Peak_Data$Time,slow,"Time Vs Transaction Slow Percentage","Percentage")
  }
  else
  {
    kpis<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
    kpis<-kpis[which(kpis %in% names(Peak_Data))]
    kpis<-kpis[!grepl("TXN|Txn",kpis)][1:4]
    if(length(kpis)>=1 && (!is.na(kpis[3])))
    {
      unit<-as.character(subset(KPI_Data,KPI_NAME==kpis[3],5))
      dyg<-shade_range(filtered,Peak_Result,Peak_Result$Time,kpis[3],paste("Time Vs",kpis[3]),unit)
    }
    else{return(NULL)}
  }
})

# output$plot4<-renderDygraph({
#   kpi<-kpi_name()
#	filtered<-data.frame(Raw_Data_inputs()$filtered)
#	Peak_Data<-data.frame(Peak_data_input()$Peak_input)
#	corelation<-data.frame(Peak_data_input()$corelation_input)
# if(input$kpi_input=="Transactions")
# {
#   txns<-unlist(trim(strsplit(corelation[cocorelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
#    txns<-txns[grep("TXN|Txn",txns)[1:5]]
#   tmd<-txns[grep("TxnTimedOutPercentage",txns)]
#	dyg<-Plot_Peaks(filtered,Peak_Data,Peak_Data$Time,tmd,"Time Vs Transaction TimedOut Percentage","Percentage")
#  }
#  else
#  {
#      kpis<-unlist(trim(strsplit(corelation[corelation$KPI_NAME==kpi,"TXN_PLOT_ORDER_STRING"],',')))
#      kpis<-kpis[!grepl("TXN|Txn",kpis)][1:4]
#      if(length(kpis)>=1 && (!is.na(kpis[4])))
#      {
#       unit<-as.character(subset(KPI_Data,KPI_NAME==kpis[4],5))
#       dyg<-Plot_Peaks(filtered,Peak_Data,Peak_Data$Time,kpis[4],paste("Time Vs",kpis[4]),unit)
#      }
#      else{return(NULL)}
#  }
# })