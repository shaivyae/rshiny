output$messageMenu <- renderMenu({
  App_Refresh_Summary<-summary_input()$App_Refresh_Summary
  msgs <- apply(App_Refresh_Summary, 1, function(x) {
    notificationItem(text = paste("Total Anomaly in",x[["App_Name"]],":",x[["Tot_Anamoly"]]),status = "warning")
  })
  dropdownMenu(type = "notifications", .list = msgs)
})