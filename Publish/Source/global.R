##### Libraries

library(RMySQL)
library(shiny)
library(shinydashboard)
library(rhandsontable)
library(gdata)
library(grid)
library(ggplot2)
library(maps)
library(plyr)
library(dplyr)
library(zoo)
library(xts)
library(doParallel)
library(plotly)
library(reshape2)
library(dygraphs)
library(RColorBrewer)
library(DT)
library(jsonlite)
#library(AnomalyProfile)
library(shinyjs)
library(V8)
library(forecast)
library(timeSeries)
library(highcharter)
library(tcltk2)
library(Rcpp)
library(visNetwork)
library(taRifx)
library(lubridate)



#### User Settings ####

client_name<-"IndusInd"
apps <- c("RIB","Core_Banking",'NetBanking')
dbuser="root"
dbpwd=""
dbnames="APPNOMIC"
dbhost="192.168.13.82"
dbport=3306

## Java Service Host Name
host_ip<-"192.168.13.83"

## DB settings

dbc<-function(){
all_cons <- dbListConnections(MySQL())
for(con in all_cons) {dbDisconnect(con)}
tryCatch({
con <- dbConnect(dbDriver("MySQL"),user=dbuser, password=dbpwd,dbname=dbnames, host=dbhost, port= dbport)
},
error=function(e) {})
}
con<-dbc()

## Txns Kpi's
txn_kpis<-"Total,AvgRespTime,SlowPercentage"

### KPIS COLS
host_kpi_cols<-c('CPU_UTIL','MEM_UTIL','DISK_IOREAD','DISK_IOWRITE','EC_CPU_UTIL','LOAD_AVERAGE')
apache_kpi_cols<-c("CPU_LOAD","BUSY_WORKERS",'TOTAL_ACCESSES','IDLE_WORKERS','TOTAL_REQUESTS','TOTAL_KBYTES_PER_REQUEST','TOTAL_KBYTES_SERVED')
oracle_kpi_cols<-c('BYTES_RECEIVED','BYTES_SENT','CURRENT_OPEN_CURSORS','CURRENT_LOGONS','CACHE_HITRATIO','PHYSICAL_READS','PHYSICAL_WRITES','LOGICAL_READS','USER_COMMITS','USER_CALLS','USER_ROLLBACKS','EXECUTE_COUNT','NUMBEROF_SESSIONS','CPU_USAGE','HARD_PARSECOUNT','SORTS_DISK','SORTS_ROWS','TABLESCANS_LONG','TABLESCANS_SHORT','LOCKS_WAITS')
websphere_kpi_cols<-c('JVM_HEAPSIZE','JVM_USEDMEMORY','JVM_CPUUSAGE','THREADPOOL_CREATECOUNT','THREADPOOL_DESTROYCOUNT','THREADPOOL_ACTIVECOUNT','THREADPOOL_POOLSIZE','CONNECTIONPOOL_POOLSIZE','CONNECTIONPOOL_FREEPOOLSIZE','CONNECTIONPOOL_PERCENTUSED','CONNECTIONPOOL_WAITTIME','CONNECTIONPOOL_CREATECOUNT','CONNECTIONPOOL_CLOSECOUNT','CONNECTIONPOOL_WAITINGTHREADCOUNT','CONNECTIONPOOL_USETIME','TOTAL_TRANSACTIONS_ACTIVE','TOTAL_TRANSACTIONS_COMMITTED','TOTAL_TRANSACTIONS_ROLLEDBACK','TOTAL_SESSIONS_CREATED','TOTAL_SESSIONS_INVALIDATED','TOTAL_SESSIONS_LIVE','TOTAL_SERVLETS_LOADED','TOTAL_SERVLETS_RELOADED','EJB_CREATED','EJB_REMOVED','EJB_PASSIVATED','EJB_METHODCALL','EJB_METHODRESPONSE_TIME')

#### Global Variables

setwd("../..")
Base_dir <- getwd()
Source_dir<-file.path(Base_dir,"Publish/Source")
source(sprintf("%s/shade_range.R",Source_dir))
source(sprintf("%s/post_proc.R",Source_dir))
source(sprintf("%s/Plot_Peaks.R",Source_dir))
source(sprintf("%s/Peak_summary.R",Source_dir))
source(sprintf("%s/Dashboard_Summary.R",Source_dir))
source(sprintf("%s/Fetch_App_Data_JService.R",Source_dir))
source(sprintf("%s/profile_anomaly.R",Source_dir))
source(sprintf("%s/Subsetting.R",Source_dir))
KPI_Data<-readRDS(sprintf("%s/Publish/Data/KPI_Data.rds",Base_dir))
texts<-c("A","B","C","D","E")
## Hour Interval (Value is in Hours)
win_len<-c("1 Hour",paste(2:4,"Hours"))


####UI Default Setting

UI_Default_Color<-"blue"
UI_Threshold<-14
# Colour to interpolate
mypal <- colorRampPalette( brewer.pal( 6 , "Spectral" ) )


### DB Connections

#getConnection <- function(group) {
#
#  if (!exists('.connection', where=.GlobalEnv)) {
#    .connection <<- dbConnect(MySQL(), group=group)
#  } else if (class(try(dbGetQuery(.connection, "SELECT 1"))) == "try-error") {
#    dbDisconnect(.connection)
#    .connection <<- dbConnect(MySQL(), group=group)
#  }
#  return(.connection)
#}
